#!/usr/bin/perl
# run as a CGI script, or offline for testing as
# vulsearch --offline SEARCH
# where SEARCH is the string to search for.


use Cwd 'abs_path';
use File::Basename;
use lib dirname( abs_path $0 );

use CGI qw(param);

#($basepath,$csspath)=($ARGV[0] ne "--offline") ? (".","/") :
($basepath,$csspath)=($ARGV[0] ne "--offline") ? ("../htdocs","/") :
    ("../htdocs", "../htdocs/" );

#initialize our hash of book names
%bib =
(
  Gn => 1,
  Ex => 2,
  Lv => 3,
  Nm => 4,
  Dt => 5,
  Jos => 6,
  Jdc => 7,
  Rt => 8,
  '1Rg' => 9,
  '2Rg' => 10,
  '3Rg' => 11,
  '4Rg' => 12,
  '1Par' => 13,
  '2Par' => 14,
  Esr => 15,
  Neh => 16,
  Tob => 17,
  Jdt => 18,
  Est => 19,
  Job => 20,
  Ps => 21,
  Pr => 22,
  Ecl => 23,
  Ct => 24,
  Sap => 25,
  Sir => 26,
  Is => 27,
  Jr => 28,
  Lam => 29,
  Bar => 30,
  Ez => 31,
  Dn => 32,
  Os => 33,
  Joel => 34,
  Am => 35,
  Abd => 36,
  Jon => 37,
  Mch => 38,
  Nah => 39,
  Hab => 40,
  Soph => 41,
  Agg => 42,
  Zach => 43,
  Mal => 44,
  '1Mcc' => 45,
  '2Mcc' => 46,
  Mt => 47,
  Mc => 48,
  Lc => 49,
  Jo => 50,
  Act => 51,
  Rom => 52,
  '1Cor' => 53,
  '2Cor' => 54,
  Gal => 55,
  Eph => 56,
  Phlp => 57,
  Col => 58,
  '1Thes' => 59,
  '2Thes' => 60,
  '1Tim' => 61,
  '2Tim' => 62,
  Tit => 63,
  Phlm => 64,
  Hbr => 65,
  Jac => 66,
  '1Ptr' => 67,
  '2Ptr' => 68,
  '1Jo' => 69,
  '2Jo' => 70,
  '3Jo' => 71,
  Jud => 72,
  Apc => 73
);

$searchstring=param("searchtext");# if ( $searchstring eq "" );
$searchstring=$ARGV[1] if $ARGV[0] eq "--offline";

#write the start of the HTML document
print "Content-type: text/html\n\n";
print <<ENDHEADER;
<!DOCTYPE html>
<html lang="en">
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <title>The Clementine Vulgate Project</title>
  <link rel="alternate" type="application/rss+xml" href="http://vulsearch.sourceforge.net/clemtext.rss" />
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
  <link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-blue.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <style>
html,body,h1,h2,h3,h4,h5,h6 {font-family: "Roboto", sans-serif; }
#content { float: left; max-width: 60ch; padding-left: 0.5ch; padding-right: 0.5ch; }
h1,h2,h3,h4,h5,h6 { color: blue; }
a { text-decoration: none; color: navy;
}
.w3-sidebar {
  z-index: 3;
  width: 250px;
  top: 43px;
  bottom: 0;
  height: inherit;
}
  </style>


  <body>
    <div id="content">

    <!-- Navbar -->
    <div class="w3-top">
      <div class="w3-bar w3-theme w3-top w3-left-align w3-large">
        <a class="w3-bar-item w3-button w3-right w3-hide-large w3-hover-white w3-large w3-theme-l1" href="javascript:void(0)" onclick="w3_open()"><i class="fa fa-bars"></i></a>
        <a href="/index.html" class="w3-bar-item w3-button w3-theme-l1">Home</a>
        <a href="/html/index.html" class="w3-bar-item w3-button w3-hover-white">View</a>
        <a href="/cgi-bin/vulsearch" class="w3-bar-item w3-button w3-hover-white">Search</a>
        <a href="/vulgata.mobi" class="w3-bar-item w3-button w3-hide-small w3-hover-white">Ebook: Kindle</a>
        <a href="/vulgata.epub" class="w3-bar-item w3-button w3-hide-small w3-hover-white">Ebook: EPUB</a>
        <a href="/vulgata-2col.pdf" class="w3-bar-item w3-button w3-hide-small w3-hover-white">PDF</a>
        <a href="#vulsearch" class="w3-bar-item w3-button w3-hide-small w3-hover-white">VulSearch</a>
        <a href="#contact" class="w3-bar-item w3-button w3-hide-small w3-hover-white">Contact</a>
      </div>
    </div>

    <!-- Sidebar -->
    <nav class="w3-sidebar w3-bar-block w3-collapse w3-large w3-theme-l5 w3-animate-left" id="mySidebar">
      <a href="javascript:void(0)" onclick="w3_close()" class="w3-right w3-xlarge w3-padding-large w3-hover-black w3-hide-large" title="Close Menu">
        <i class="fa fa-remove"></i>
      </a>
      <a href="/index.html" class="w3-bar-item w3-button w3-hover-white">Home</a>
      <a href="/html/index.html" class="w3-bar-item w3-button w3-hover-white">View text online</a>
      <a href="/vulgata.epub" class="w3-bar-item w3-button w3-hover-white">Download ebook</a>
      <a href="/cgi-bin/vulsearch" class="w3-bar-item w3-button w3-hover-white">Search text</a>
      <a href="#vulsearch" class="w3-bar-item w3-button w3-hover-white">VulSearch for Windows</a>
      <a href="#contact" class="w3-bar-item w3-button w3-hover-white">Contact</a>
    </nav>

    <!-- Overlay effect when opening sidebar on small screens -->
    <div class="w3-overlay w3-hide-large" onclick="w3_close()" style="cursor:pointer" title="close side menu" id="myOverlay"></div>

  <!-- Header -->
  <header class="w3-container w3-padding-48 w3-white">
    <h1><b>Clementine Vulgate Project</b></h1>
    <h6><span class="w3-tag w3-blue">The full text of the Clementine Vulgate, freely available online</span></h6>
  </header>

    <!-- Main content: shift it to the right by 250 pixels when the sidebar is visible -->
    <div class="w3-main" style="margin-left:250px">

      <div class="w3-row">
        <h2>Search the Clementine Vulgate</h2>
ENDHEADER

#the heart of the program
sub cleanup
{
  $s=$_[0];
  $s =~ s/ :/\&nbsp;:/g;
  $s =~ s/ ;/\&nbsp;;/g;
  $s =~ s/ \?/\&nbsp;?/g;
  $s =~ s/ !/\&nbsp;!/g;
  $s =~ s/\xe6/\&aelig;/g;
  $s =~ s/\xeb/\&euml;/g;
  $s =~ s/\xc6/\&AElig;/g;
  $s =~ s/\x9c/\&oelig;/g;
  $s =~ s/\x8c/\&OElig;/g;
  return $s;
}


if ($searchstring ne '')
{
  $results=`grep -iHE "$searchstring" $basepath/txt/*.txt`;
  $results= cleanup($results);

  @verses=split "\n", $results;
  $t=0;
  foreach $verse (@verses)
  {
    ($head, $body) = split " ", $verse, 2;
    ($_, $book) = split /.+\//, $head;
    ($book, $_, $chap, $ver) = split /[\.\:]/, $book;
    $books[$t]=$book;
    $chaps[$t]=$chap;
    $vers[$t]=$ver;
    $body[$t]=$body;
    $ct[$t]=$t;
    $t++;
  }

  print '<h3>Search results for ', "$searchstring ($t)", '</h3>', "\n";


  sub sorting
  {
    $bib{$books[$a]} <=> $bib{$books[$b]} or
    $chaps[$a] <=> $chaps[$b] or
    $vers[$a] <=> $vers[$b]
  }

  if ($t==0)
  {
    print '<p>No results found.</p>', "\n";
  }
  else
  {

    @ct = sort sorting @ct;
    for ($i=0; $i<$t; $i++)
    {
      $href='<a href="' . "../html/$books[$ct[$i]].html#x$chaps[$ct[$i]]_$vers[$ct[$i]]". '">';
      print '<p>', $href, '<span class="ref">',
      "$books[$ct[$i]] $chaps[$ct[$i]]:$vers[$ct[$i]]", '</span></a> ', $body[$ct[$i]], '</p>', "\n\n";
    }
  }
}

#always provide a new search option
print <<ENDFOOTER;
          <h3>New search</h3>
          <form action="http://vulsearch.sourceforge.net/cgi-bin/vulsearch"
          method="post">
          <p><input type="text" name="searchtext" value="$searchstring" /> <input
          type="submit" name="searchbutton" value="Search" />
          </p></form>

          <h3>Instructions</h3>
          <p>As well as typing in simple phrases to search for, you can perform
          sophisticated searches using regular expressions.</p>
          <ul>
          <li> To search for &aelig; or other ligatures, replace them with periods (for
          example, <span class="ref">s.culum s.culi</span>).  </li>
          <li> To search for whole words, put \\b before and after the word (for
          example, <span class="ref">\\bpatre\\b</span>).  </li>
          <li> To search for either a or b, use vertical bars in parentheses (for
          example, <span class="ref">virg(o|in)</span>).  </li>
          </ul>
        </div>

        <footer id="myFooter">
          <p style="padding-top: 4em;"><i>Hosted by</i>
          <a
             href="http://sourceforge.net"><img style="border: 0;"
                                                src="http://sourceforge.net/sflogo.php?group_id=53472&amp;type=1"
                                                alt="SourceForge.net" id="sf" /></a>
          </p>
        </footer>

        <!-- END MAIN -->
      </div>

      <script>
// Get the Sidebar
var mySidebar = document.getElementById("mySidebar");

// Get the DIV with overlay effect
var overlayBg = document.getElementById("myOverlay");

// Toggle between showing and hiding the sidebar, and add overlay effect
function w3_open() {
  if (mySidebar.style.display === 'block') {
    mySidebar.style.display = 'none';
    overlayBg.style.display = "none";
  } else {
    mySidebar.style.display = 'block';
    overlayBg.style.display = "block";
  }
}

// Close the sidebar with the close button
function w3_close() {
  mySidebar.style.display = "none";
  overlayBg.style.display = "none";
}
      </script>

  </body>


</html>
ENDFOOTER

